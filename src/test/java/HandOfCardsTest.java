import idatt2001.oblig3.DeckOfCards;
import idatt2001.oblig3.HandOfCards;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HandOfCardsTest {
    
    HandOfCards hand;

    @BeforeEach
    void setUp() {
        hand = new HandOfCards(5, new DeckOfCards());
    }

    @Test
    void noDuplicateCards() {
        assertEquals(hand.getHand().stream().distinct().count(), 5);
    }
}
