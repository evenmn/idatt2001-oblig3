import idatt2001.oblig3.DeckOfCards;
import idatt2001.oblig3.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeckOfCardsTest {

    DeckOfCards deck;
    ArrayList<PlayingCard> hand;

    @BeforeEach
    void setUp() {
        deck = new DeckOfCards();
    }

    @Test
    void deckSizeIs52() {
        assertEquals(deck.getDeckSize(), 52);
    }

    @Test
    void deckContainsFourSuits() {
        assertEquals(deck.getDeck().stream().filter(p -> p.getSuit() == 'D').count(), 13);
        assertEquals(deck.getDeck().stream().filter(p -> p.getSuit() == 'C').count(), 13);
        assertEquals(deck.getDeck().stream().filter(p -> p.getSuit() == 'H').count(), 13);
        assertEquals(deck.getDeck().stream().filter(p -> p.getSuit() == 'S').count(), 13);
    }

    @Test
    void noDuplicateCards() {
        assertEquals(deck.getDeck().stream().distinct().count(), 52);
    }

    @Test
    void handIsRemovedFromDeck() {
        hand = deck.dealHand(7);
        assertEquals(deck.getDeckSize(), 45);
    }
}
