package idatt2001.oblig3.gui;

import idatt2001.oblig3.HandOfCards;
import idatt2001.oblig3.PlayingCard;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;


import java.awt.*;
import java.util.ArrayList;


public class Controller {
    Scene scene;
    ImageView table = new ImageView(Assets.getAssetsInstance().getTable());
    BorderPane layout;
    ArrayList<ImageView> cardImages;
    final int CARD_WIDTH = 100;
    final int TABLE_WIDTH = 1000;
    HandOfCards hand;
    String isFlush;
    String hasQueenOfSpades;
    Button deal;
    String cardsOfHearts = "";

    public Controller(HandOfCards hand, Button deal) {
        this.hand = hand;
        this.deal = deal;
        table.setPreserveRatio(true);
        table.setFitWidth(TABLE_WIDTH);

        checkHand();

        constructLayout();
        scene = new Scene(layout);
    }

    /**
     * The layout of the scene is updated to go with the values associated with the current hand.
     */
    private void constructLayout() {
        layout = new BorderPane();

        HBox cards = new HBox();
        cards.setSpacing((table.getFitWidth() * 0.6 - CARD_WIDTH * hand.getHandSize()) / (hand.getHandSize() - 1));
        cards.setAlignment(Pos.CENTER);
        cardImages.forEach(s -> cards.getChildren().add(s));

        StackPane center = new StackPane();
        center.getChildren().addAll(table, cards);
        layout.setCenter(center);

        VBox top = new VBox();
        top.setPadding(new Insets(10, 10, 10, 10));
        top.setSpacing(10);
        top.setAlignment(Pos.TOP_CENTER);

        Label title = new Label("Poker");
        title.setAlignment(Pos.CENTER);
        title.setFont(new Font(30));

        top.getChildren().addAll(title, deal);
        layout.setTop(top);

        GridPane bottom = new GridPane();
        bottom.setPadding(new Insets(10, 10, 50, 10));
        bottom.setHgap(10);
        bottom.setVgap(5);
        bottom.setAlignment(Pos.CENTER);

        VBox bottomVBox1 = new VBox();
        GridPane.setConstraints(bottomVBox1,0,0);
        bottomVBox1.setMinWidth(180);
        bottomVBox1.setAlignment(Pos.CENTER_RIGHT);

        VBox bottomVBox2 = new VBox();
        GridPane.setConstraints(bottomVBox2,1,0);
        bottomVBox2.setMinWidth(20);
        bottomVBox2.setAlignment(Pos.CENTER_LEFT);

        VBox bottomVBoxEmpty = new VBox();
        GridPane.setConstraints(bottomVBoxEmpty, 2, 0);
        bottomVBoxEmpty.setMinWidth(100);

        VBox bottomVBox3 = new VBox();
        GridPane.setConstraints(bottomVBox3,3,0);
        bottomVBox3.setMinWidth(100);
        bottomVBox3.setAlignment(Pos.CENTER_RIGHT);

        VBox bottomVBox4 = new VBox();
        GridPane.setConstraints(bottomVBox4, 4,0);
        bottomVBox4.setMinWidth(100);
        bottomVBox4.setAlignment(Pos.CENTER_LEFT);

        Label label1 = new Label("SUM OF THE FACES:");
        Label label2 = new Label("FLUSH:");
        bottomVBox1.getChildren().addAll(label1, label2);

        Label label3 = new Label(Integer.toString(hand.getSumOfCards()));
        Label label4 = new Label(isFlush);
        bottomVBox2.getChildren().addAll(label3, label4);

        Label label5 = new Label("CARDS OF HEARTS:");
        Label label6 = new Label("QUEEN OF SPADES:");
        bottomVBox3.getChildren().addAll(label5, label6);

        Label label7 = new Label(cardsOfHearts);
        Label label8 = new Label(hasQueenOfSpades);
        bottomVBox4.getChildren().addAll(label7, label8);

        bottom.getChildren().addAll(bottomVBox1, bottomVBox2, bottomVBoxEmpty, bottomVBox3, bottomVBox4);
        layout.setBottom(bottom);
    }

    public Scene getScene() {
        return scene;
    }

    /**
     * Reconstructs the scene when a new hand is dealt
     * @param hand Current hand of cards
     */
    public void newHand(HandOfCards hand) {
        this.hand = hand;
        checkHand();
        constructLayout();
        scene.setRoot(layout);
    }

    /**
     * Checks the current hand for 5-hand flush, queen of spades, and cards of the heart suit, and updates the associated variables.
     * The list of card images that are to be displayed is also updated
     */
    public void checkHand() {
        cardImages = new ArrayList<>();
        for (PlayingCard card : hand.getHand()) {
            cardImages.add(new ImageView(Assets.getAssetsInstance().getCards().get(Integer.toString(card.getFace()) + card.getSuit())));
            cardImages.get(cardImages.size() - 1).setPreserveRatio(true);
            cardImages.get(cardImages.size() - 1).setFitWidth(CARD_WIDTH);
        }

        if (hand.checkForFiveCardFlush()) {
            isFlush = "YES";
        } else {
            isFlush = "NO";
        }

        if (hand.containsQueenOfSpades()) {
            hasQueenOfSpades = "YES";
        } else {
            hasQueenOfSpades = "NO";
        }

        cardsOfHearts = "";
        hand.getHearts().forEach(s -> cardsOfHearts += s.getAsString() + ", ");
    }
}
