package idatt2001.oblig3.gui;

import javafx.scene.image.Image;
import java.util.HashMap;

public class Assets {

    //This class contains methods to get the image-files used in the program.
    //An object instance of the class can be accessed anywhere using the Singleton design-pattern.

    private static final Assets assetsInstance = new Assets();
    private HashMap<String, Image> cards;
    private final Image table;

    private Assets() {
        table = new Image("/textures/Table.png");
        cards = new HashMap<>();
        char[] suits = {'D', 'C', 'H', 'S'};
        for (char suit : suits) {
            for (int i = 1; i <= 13; i++) {
                cards.put(Integer.toString(i) + suit, new Image("/textures/Cards/" + i + suit + ".png"));
            }
        }
    }

    public static Assets getAssetsInstance() {
        return assetsInstance;
    }

    public HashMap<String, Image> getCards() {
        return cards;
    }

    public Image getTable() {
        return table;
    }
}
