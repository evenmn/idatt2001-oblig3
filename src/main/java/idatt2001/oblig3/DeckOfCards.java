package idatt2001.oblig3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {

    private final char[] suits = {'D', 'C', 'H', 'S'};
    private List<PlayingCard> deck;
    private Random random = new Random();

    /**
     * Creates a deck of 52 playing cards, collected in a list
     */
    public DeckOfCards() {
        deck = new ArrayList<>();
        for (char suit : suits) {
            for (int i = 1; i <= 13; i++) {
                deck.add(new PlayingCard(suit, i));
            }
        }
    }

    /**
     * Picks a number of cards from the deck and returns them in a list.
     * @param n The number of cards the method should draw
     * @return List of playing cards
     * @throws ArithmeticException Method cannot deal a number of cards that exceeds the number of cards in the deck or a negative number of cards.
     */
    public ArrayList<PlayingCard> dealHand(int n) throws ArithmeticException {
        if (n < 0 || n > deck.size()) {
            throw new ArithmeticException("Deck cannot deal this amount of cards");
        }
        ArrayList<PlayingCard> dealtCards = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int randomNumber = random.nextInt(deck.size());
            dealtCards.add(deck.get(randomNumber));
            deck.remove(randomNumber);
        }
        return dealtCards;
    }

    public int getDeckSize() {
        return deck.size();
    }

    public List<PlayingCard> getDeck() {
        return deck;
    }
}
