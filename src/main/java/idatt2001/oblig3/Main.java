package idatt2001.oblig3;

import idatt2001.oblig3.gui.Controller;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    Stage stage;
    Controller sceneBuilder;

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle("Poker Game");
        Button deal = new Button("Deal Hand");
        deal.setAlignment(Pos.CENTER);
        deal.setFont(new Font(20));
        try {
            sceneBuilder = new Controller(new HandOfCards(5, new DeckOfCards()), deal);
        } catch (ArithmeticException exception) {
            exception.printStackTrace();
        }
        deal.setOnAction(e -> {
            try {
                sceneBuilder.newHand(new HandOfCards(5, new DeckOfCards()));
            } catch (ArithmeticException exception) {
                exception.printStackTrace();
            }
        });
        stage.setScene(sceneBuilder.getScene());
        stage.show();
    }
}


