package idatt2001.oblig3;

import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {

    private int handSize;
    private List<PlayingCard> hand;
    private final char[] suits = {'D', 'C', 'H', 'S'};

    public HandOfCards(int handSize, DeckOfCards deck) throws ArithmeticException{
        this.handSize = handSize;
        hand = deck.dealHand(handSize);
    }

    /**
     * Calculates the sum of the cards faces in a hand (ace counts as 1)
     * @return Sum
     */
    public int getSumOfCards() {
        return hand.stream().map(PlayingCard::getFace).reduce(Integer::sum).get();
    }

    /**
     * Checks if the card queen of spades is present in the hand
     * @return True if hand contains queen of spades, false if it does not
     */
    public boolean containsQueenOfSpades() {
        PlayingCard card = new PlayingCard('S', 12);
        return hand.stream().anyMatch(p -> p.equals(card));
    }

    /**
     * Checks if the hand contains five or more cards of the same suit.
     * @return True if hand does contain a five hand flush, false if it does not.
     */
    public boolean checkForFiveCardFlush() {
        for (char suit : suits) {
            if (hand.stream().filter(p -> p.getSuit() == suit).count() >= 5) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks hand for all cards of the heart suit.
     * @return A list of all cards of the heart suit on hand
     */
    public List<PlayingCard> getHearts() {
        return hand.stream().filter(p -> p.getSuit() == 'H').collect(Collectors.toList());
    }

    public int getHandSize() {
        return handSize;
    }

    public List<PlayingCard> getHand() {
        return hand;
    }
}
